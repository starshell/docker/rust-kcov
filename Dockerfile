FROM rust

ENV PATH ${CARGO_HOME:-$HOME/.cargo}/bin:$PATH

RUN echo "CARGO_HOME: ${CARGO_HOME:-$HOME/.cargo}/bin" \
    && mkdir -p ${CARGO_HOME:-$HOME/.cargo}/bin

RUN rustup default stable \
    && rustup component add rustfmt-preview \
    && apt-get update && apt-get install --yes \
        cmake \
        g++ \
        pkg-config \
        jq \
        libcurl4-openssl-dev \
        libelf-dev \
        libdw-dev \
        binutils-dev \
        libiberty-dev \
    && cargo install cargo-kcov \
    && cargo kcov --print-install-kcov-sh | sh \
    && rm -rf /var/lib/apt/lists/* \
    && kcov --version \
    && cargo kcov --version
