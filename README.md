# Rust Kcov 

Stable: [![pipeline status](https://gitlab.com/starshell/docker/rust-kcov/badges/stable/pipeline.svg)](https://gitlab.com/starshell/docker/rust-kcov/commits/stable) Beta: [![pipeline status](https://gitlab.com/starshell/docker/rust-kcov/badges/beta/pipeline.svg)](https://gitlab.com/starshell/docker/rust-kcov/commits/beta) Nightly: [![pipeline status](https://gitlab.com/starshell/docker/rust-kcov/badges/nightly/pipeline.svg)](https://gitlab.com/starshell/docker/rust-kcov/commits/nightly)

<img alt="docker" src="http://iron-oxide.gitlab.io/icons/logos/docker.svg" width="100" height="100" /> <img alt="rust" src="http://iron-oxide.gitlab.io/icons/logos/rust.svg" width="100" height="100" />

[Official Rust](https://hub.docker.com/_/rust/) with [Kcov](https://github.com/SimonKagstrom/kcov).

> For generating test coverage reports.

All builds are automated so they are always the most recent images. The nightly image rebuilds every day, both beta and nightly rebuild once a week.

## Usage

The images are hosted on GitLab registries.

### GitLab

All of the images can be found in the project [registry](https://gitlab.com/starshell/docker/rust-kcov/container_registry) and can be used just like images from dockerhub.

    docker pull registry.gitlab.com/starshell/docker/rust-kcov:stable
    docker pull registry.gitlab.com/starshell/docker/rust-kcov:beta
    docker pull registry.gitlab.com/starshell/docker/rust-kcov:nightly

To use with CI just replace `image: rust` with the URL of the registry image.

```yaml
.cargo_test_template: &cargo_test
  stage: test
  script:
    - cargo test --verbose --jobs 1 --all
  after_script:
    - cargo kcov --verbose --all
    - cat target/cov/index.json

test:stable:cargo:
  image: registry.gitlab.com/starshell/docker/rust-kcov:stable
  <<: *cargo_test

test:beta:cargo:
  image: registry.gitlab.com/starshell/docker/rust-kcov:beta
  <<: *cargo_test

test:nightly:cargo:
  image: registry.gitlab.com/starshell/docker/rust-kcov:nightly
  <<: *cargo_test
```

The regex to use for GitLab CI is:

```
"covered":"(\d+(?:\.\d+)?)",
```

Example output `target/cov/index.json`:

```json
var data = {files:[
], merged_files:[{"link":"kcov-merged/index.html","title":"[merged]","summary_name":"[merged]","covered_class":"lineNoCov","covered":"0.1","covered_lines":"4","uncovered_lines":"3058","total_lines" : "3062"},
]};
var percent_low = 25;var percent_high = 75;
var header = { "command" : "", "date" : "2018-09-23 17:30:23", "instrumented" : 3062, "covered" : 4,};
```

You can test it at [http://rubular.com/](http://rubular.com/).

## Reference

Follow the discussion: [https://users.rust-lang.org/t/650](https://users.rust-lang.org/t/650)

&nbsp;

This may simplify installation:

```bash
sudo apt-get install cmake g++ pkg-config jq
sudo apt-get install libcurl4-openssl-dev libelf-dev libdw-dev binutils-dev libiberty-dev
cargo kcov --print-install-kcov-sh | sh
```
